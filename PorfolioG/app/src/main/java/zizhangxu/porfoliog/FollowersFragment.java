package zizhangxu.porfoliog;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import zizhangxu.porfoliog.pojo.Profile;
import zizhangxu.porfoliog.pojo.Repos;


public class FollowersFragment extends Fragment {

    public static final String BASE_URL = "http://api.github.com/";
    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    String user_name;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_repositories, container, false);

        user_name = "DarronXu";

        GithubApi apiService =
                retrofit.create(GithubApi.class);
        Call<List<Profile>> call = apiService.getFollowers(user_name);
        call.enqueue(new Callback<List<Profile>>() {
            @Override
            public void onResponse(Call<List<Profile>> call, Response<List<Profile>> response) {
                int statusCode = response.code();
                List<Profile> profiles = response.body();
                Log.d("prof","success");
                // set repos adapter
                if (view instanceof RecyclerView){
                    Context context = view.getContext();
                    RecyclerView recyclerView = (RecyclerView) view;

                    recyclerView.setLayoutManager(new LinearLayoutManager(context));

                    recyclerView.setAdapter(new ProfileAdapter(profiles, false));
                }


                String FILENAME = "followers_list";

                Gson gson = new GsonBuilder().create();
                String repos_json = gson.toJson(profiles);

                try {
                    FileOutputStream fos = getActivity().openFileOutput(FILENAME, Context.MODE_PRIVATE);
                    fos.write(repos_json.getBytes());
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<List<Profile>> call, Throwable t) {
                // Log error here since request failed
                Log.d("repo", "fail to connect");
            }
        });


        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Followers");
    }
}
