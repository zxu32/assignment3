package zizhangxu.porfoliog;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.HEAD;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import zizhangxu.porfoliog.pojo.Profile;
import zizhangxu.porfoliog.pojo.Repos;

public interface GithubApi {

    @GET("users/{username}")
    Call<Profile> getUser(@Path("username") String username);

    @GET("user")
    Call<Profile> getAuthUser();

    @GET("/users/{username}/repos")
    Call<List<Repos>> getRepos(@Path("username") String username);

    @GET("/users/{username}/following")
    Call<List<Profile>> getFollowing(@Path("username") String username);

    @GET("/users/{username}/followers")
    Call<List<Profile>> getFollowers(@Path("username") String username);

    @Headers("Authorization: Basic RGFycm9uWHU6eHp6OTUwODA3")
    @PUT("/user/following/{username}")
    Call<String> followUser(@Path("username") String username);

    @Headers("Authorization: Basic RGFycm9uWHU6eHp6OTUwODA3")
    @DELETE("/user/following/{username}")
    Call<String> unFollowUser(@Path("username") String username);

    @Headers("Authorization: Basic RGFycm9uWHU6eHp6OTUwODA3")
    @PUT("/user/starred/{owner}/{repo}")
    Call<String> starRepo(@Path("owner") String owner, @Path("repo") String repo);

    @Headers("Authorization: Basic RGFycm9uWHU6eHp6OTUwODA3")
    @DELETE("/user/starred/{owner}/{repo}")
    Call<String> unStarRepo(@Path("owner") String owner, @Path("repo") String repo);


}


