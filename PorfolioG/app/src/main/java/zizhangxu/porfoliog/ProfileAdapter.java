package zizhangxu.porfoliog;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import zizhangxu.porfoliog.pojo.Profile;


public class ProfileAdapter extends RecyclerView.Adapter<ProfileAdapter.ViewHolder> {
    private List<Profile> mProfiles;
    private boolean mIsFollowing;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public ImageView avatar;
        public TextView name;
        public ViewHolder(View v) {
            super(v);
            avatar = v.findViewById(R.id.avatar);
            name = v.findViewById(R.id.userName);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ProfileAdapter(List<Profile> profiles, Boolean isFollowing) {
        mProfiles = profiles;
        mIsFollowing = isFollowing;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ProfileAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
        // create a new view
        View v;
        if(mIsFollowing)
            v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.following, parent, false);
        else
            v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.followers, parent, false);


        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        //holder.
        Profile profile = mProfiles.get(position);
        //holder.avatar.setImageURI(Uri.parse(profile.getAvatarUrl()));


        new ImageLoadTask(profile.getAvatarUrl(), holder.avatar).execute();


        holder.name.setText(profile.getLogin());

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mProfiles.size();
    }
}