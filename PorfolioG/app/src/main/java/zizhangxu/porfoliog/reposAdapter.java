package zizhangxu.porfoliog;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import zizhangxu.porfoliog.pojo.Profile;
import zizhangxu.porfoliog.pojo.Repos;


public class reposAdapter extends RecyclerView.Adapter<reposAdapter.ViewHolder> {
    private List<Repos> mRepos;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView repos_name;
        public TextView repos_owner;
        public TextView repos_description;
        public ViewHolder(View v) {
            super(v);
            repos_name = v.findViewById(R.id.repos_name);
            repos_owner = v.findViewById(R.id.repos_owner);
            repos_description = v.findViewById(R.id.repos_description);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public reposAdapter(List<Repos> repos) {
        mRepos = repos;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public reposAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.repos, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        //holder.
        Repos repo = mRepos.get(position);
        holder.repos_name.setText(repo.getName());
        holder.repos_description.setText(repo.getDescription());
        Profile owner = repo.getOwner();
        holder.repos_owner.setText(owner.getLogin());

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mRepos.size();
    }
}