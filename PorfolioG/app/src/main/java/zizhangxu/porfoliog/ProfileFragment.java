package zizhangxu.porfoliog;

import android.content.Context;
import android.net.Credentials;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.w3c.dom.Text;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import zizhangxu.porfoliog.pojo.Profile;

public class ProfileFragment extends Fragment {
    private static final String ARG_USER_NAME = "DarronXu";


    public static final String BASE_URL = "http://api.github.com/";
    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    String user_name = "DarronXu";

    public static ProfileFragment newInstance(String user_name){
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_USER_NAME, user_name);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        if(getArguments() != null){
            this.user_name = getArguments().getString(ARG_USER_NAME);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle saedInstanceState) {

        GithubApi apiService =
                retrofit.create(GithubApi.class);

        final View view = inflater.inflate(R.layout.fragment_profile, container, false);


        Call<Profile> call = apiService.getUser(user_name);
        //Call<Profile> call = apiService.getAuthUser();
        call.enqueue(new Callback<Profile>() {
            @Override
            public void onResponse(Call<Profile> call, Response<Profile> response) {

                int statusCode = response.code();
                Profile user = response.body();
                Log.d("response", response.message());


                ImageView avatar = view.findViewById(R.id.avatar);
                TextView userName = view.findViewById(R.id.userName);
                TextView name = view.findViewById(R.id.name);
                Button repos_count = view.findViewById(R.id.button_repos);
                Button following_count = view.findViewById(R.id.button_following);
                Button followers_count = view.findViewById(R.id.button_followers);
                TextView date = view.findViewById(R.id.date);


                new ImageLoadTask(user.getAvatarUrl(), avatar).execute();
                userName.setText(user.getLogin());
                name.setText(user.getName());

                String text;

                text = "Repos:" + user.getPublicRepos();
                repos_count.setText(text);


                text = "Following: " + user.getFollowing().toString();
                following_count.setText(text);

                text = "Followers: " + user.getFollowers().toString();
                followers_count.setText(text);

                date.setText(user.getCreatedAt());


                String FILENAME = "profile";

                Gson gson = new GsonBuilder().create();
                String repos_json = gson.toJson(user);

                try {
                    FileOutputStream fos = getActivity().openFileOutput(FILENAME, Context.MODE_PRIVATE);
                    fos.write(repos_json.getBytes());
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Profile> call, Throwable t) {
                // Log error here since request failed
                Log.d("API","Connection failure");
            }
        });

        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Profile");
    }



}
